exports.connector = function(){
	var azure;

	var boardTable;
	var userDataTable;
	var userId;
	var tableService;
	var query;
	var table;
	var card;
	_getUniqueId = function()
	{
		var currentDate = new Date();
		var uniqueId = currentDate.getFullYear().toString() 
		+ currentDate.getFullYear().toString() 
		+ currentDate.getMonth().toString()
		+ currentDate.getDate().toString()
		+ currentDate.getHours().toString()
		+ currentDate.getMinutes().toString()
		+ currentDate.getSeconds().toString()
		+ Math.floor(Math.random()*1001);
		return uniqueId;
	},

	_init = function(){
		console.log('loading azure module');
		azure = require('azure');
		//process.env.EMULATED = "true";
		process.env.AZURE_STORAGE_ACCOUNT = 'kanbanmobile';
		process.env.AZURE_STORAGE_ACCESS_KEY = 'omR6xGddmZJJOiRzXa0fdePe8AYeAOhHCopRweACXUoQiPenC98b5JdiUrh9h6PBhRZFRLc+VO+DHUqmIwMjvQ==';
		
		if (!process.env.AZURE_STORAGE_ACCOUNT) {
			process.env.EMULATED = "true";
			console.log("Running Under Emulation :" + process.env.EMULATED);
		} else { 
			console.log("Running against " + process.env.AZURE_STORAGE_ACCOUNT);
		}
		
		
		card = 'card';
		table = 'table';
		boardTable = 'boards';
		userTable = 'userTable';
		userId = 1;
		tableService = azure.createTableService();
		
      	tableService.createTableIfNotExists(boardTable,function(err){
			if(err!=null)
				console.log('[azureConnector]-init on create board table if not exsists error: '+err);
		});
		tableService.createTableIfNotExists(userTable,function(err){
			if(err!=null)
				console.log('[azureConnector]-init on create user table if not exsists error: '+err);
		});

		tableService.createTableIfNotExists(table,function(err){
			if(err!=null)
				console.log('[azureConnector] init on create table if not exsists error: '+err);
		});
		tableService.createTableIfNotExists(card,function(err){
			if(err!=null)
				console.log('[azureConnector] init on create card table if not exsists error: '+err);
		});
    },
	_insertBoard = function(id,title,content,callback){
		console.log('[azureConnector]-befor _insertBoard');
		tableService.insertEntity(boardTable, {
			PartitionKey: id,//boardTable,
			RowKey: id + title,
			Title: title,
			Content: content,
		}, function(err){
			if(err!=null){
				console.dir('[Error azureConnector]-insert board error: '+err);
			}
			else{
				console.log('[azureConnector]-after _insertBoard');
			}
			callback(err);
		});
    },
	_deleteBoard = function(id,title,callback){
		console.log('[azureConnector]-befor _deleteBoard');
		tableService.deleteEntity(boardTable, { PartitionKey : id , RowKey : id + title },function(err){
			if(err!=null){
				console.dir('[Error azureConnector] _deleteBoard error: '+err);
			}
			console.log('azureConnector-after _deleteBoard');
		});
		
	},
	_selectBoards = function(id,callback){
		console.log('azureConnector-befor _selectBoards');
		var query = azure.TableQuery.select().from(boardTable).where('PartitionKey eq ?', id);
		tableService.queryEntities(query,function(err,results){
			if(err!=null){
				console.dir('[Error]azureConnector-after _selectBoards error: '+err);
			}
			callback(err,results);
		});
	},
	_selectUser = function(name,password,callback){
		console.log('azureConnector-befor _selectUser');
		var query = azure.TableQuery.select().from(userTable).where('Name eq ?', name);
		tableService.queryEntities(query,function(err,result){
			if(err!=null){
				console.log('[Error] azureConnector-after _selectUser: '+err);
			}
			else{
				console.log('azureConnector-after _selectUser');
			}
			callback(err,result);
		});
	},
	_insertUser = function(name,email,password,callback){
			console.log('azureConnector-befor _insertUser');
			var uniqueId = _getUniqueId();
		tableService.insertEntity(userTable, {
			PartitionKey: userTable,
			RowKey: uniqueId,
			Name: name,
			Email: email,
			Password: password
		}, function(err){
			
			if(err!=null){
				console.dir('[Error]azureConnector-insert user error: '+err);
				callback(false,'',err);
			}
			else{
				console.log('azureConnector-after _insertUser');
				callback(true,uniqueId);
			}
		});
	}
	_selectTables = function(userId,boardTitle,callback){
		console.log('[azureConnector]-befor _selectTables');
		var query = azure.TableQuery.select().from(table).where('PartitionKey eq ? and BoardTitle eq ?', userId, boardTitle);
		tableService.queryEntities(query,function(err,results){
			if(err!=null){
				console.dir('[Error]azureConnector-after _selectBoards error: '+err);
			}
			callback(err,results);
		});
	},
	_insertTable = function(userId,boardTitle,tables,callback){
		console.log('[azureConnector]-befor _insertTable');
		var uniqueId = _getUniqueId();
		tableService.insertEntity(table,{ 
				PartitionKey: userId,
				RowKey: userId + boardTitle,
				BoardTitle: boardTitle,
				Tables: tables
			}, 
			function(err){
				if(!err){
					console.log('[Error-azureConnector] after _insertTable');
				}
				console.log('[azureConnector] after _insertTable');
				callback(err);
			}
		);
	},
	_insertOrReplaceTable = function(userId,boardTitle,tables,callback){
		console.log('[azureConnector]-befor _insertOrReplaceTable');
		var uniqueId = _getUniqueId();
		tableService.insertOrReplaceEntity(table,{ 
				PartitionKey: userId,
				RowKey: userId + boardTitle,
				BoardTitle: boardTitle,
				Tables: tables
			}, 
			function(err){
				if(err!=null){
					console.log('[Error-azureConnector] after _insertOrReplaceTable');
				}
				console.log('[azureConnector] after _insertOrReplaceTable');
				callback(err);
			}
		);
		 
	},
	_insertOrReplaceCard = function(userId, boardTitle,cardObject,callback){
		console.log('[azureConnector]-befor _insertOrReplaceCard');
		var uniqueId = _getUniqueId();
		tableService.insertOrReplaceEntity('card',{ 
				PartitionKey: userId + cardObject.cardId,
				RowKey: cardObject.cardId,
				cardName: cardObject.cardName,
				cardDetails1: cardObject.cardDetails1,
				cardDetails2: cardObject.cardDetails2
			}, 
			function(err){
				if(err!=null){
					console.log('[Error-azureConnector] after _insertOrReplaceCard');
				}
				console.log('[azureConnector] after _insertOrReplaceCard');
				callback(err);
			}
		);
	},
	_selectCard = function(userId, cardId, callback){
		console.log('[azureConnector]-befor _selectCard');
		var query = azure.TableQuery.select().from(card).where('PartitionKey eq ? ', userId + cardId);
		tableService.queryEntities(query,function(err,results){
			if(err!=null){
				console.dir('[Error]azureConnector-after _selectCard error: '+err);
			}
			console.log('[azureConnector]-after _selectCard');
			callback(err,results);
		});
	}
  return {
    selectCard : _selectCard,
	insertOrReplaceCard : _insertOrReplaceCard,
	init: _init,
	insertTable : _insertTable,
	insertOrReplaceTable : _insertOrReplaceTable,
	selectTables : _selectTables,
	selectUser: _selectUser,
	insertUser: _insertUser,
	insertBoard: _insertBoard,
	deleteBoard: _deleteBoard,
    selectBoards: _selectBoards,
  }
}();
