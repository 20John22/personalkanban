var azure = require('./../azureConnector.js');
azure.connector.init();

exports.getCard = function(req,res){
	var result = azure.connector.selectCard(req.params.id, req.params.cardId,
		function(err,results){
			if(err!=null){
				console.log(err);
				res.send(err,results);
			}
			console.log(results[0]);
			res.send(JSON.stringify(results[0]));
		}
	);
}

exports.addCard = function(req,res){
	console.log(req.body);
	azure.connector.insertOrReplaceCard(req.params.id, req.body.boardTitle,req.body.cardObject,function(err){
		if(err!=null){
			console.log('[Error insertCard] Updated values to card' + err);
		}
		res.send(err);
	})

}

exports.updateTable = function(req,res){
	console.log(req.body.tables);
	azure.connector.insertOrReplaceTable(req.params.id, req.params.boardTitle,JSON.stringify(req.body.tables),function(err){
		if(err!=null){
			console.log('[Error insertBoard] Updated values to table' + err);
		}
		res.send(err);
	})
}

exports.tables = function(req,res){
	var result = azure.connector.selectTables(req.params.id, req.params.boardTitle,
		function(err,results){
			if(err!=null){
				console.log(err);
				res.send(err,results);
			}

			res.send(JSON.parse(results[0].Tables));
		}
	);
}

exports.boards = function(req,res){
	var result = azure.connector.selectBoards(req.params.id, function(err,results){
			if(err!=null){
				console.dir(err);
				res.send(err,results);
			}
			res.send(JSON.stringify(results));
		}
	);
}

exports.insertBoard = function(req,res){
	if(req.body.title=='')
	{
		res.send('undefined title');
	}
	
	var result = azure.connector.insertBoard(req.params.id, req.body.title, req.body.content,
		function(err){
			if(err!=null){
				console.log(err);
				res.send(err);
			}
			
			console.log('insert default table');
			var tables = [
                {   title: "To do",
                    cards: [
                        { id: 10, name: "Temp 1" }
                            ]
                },
                {   title: "Doing",
                    cards: [
                        { id: 8, name: "Temp 2" }
                            ]
                },
				{   title: "Done",
                    cards: [
                        { id: 8, name: "Temp 3" },
                        { id: 5, name: "Temp 4" }
                            ]
                }
              ];
			//console.log(tables);
			azure.connector.insertTable(req.params.id, req.body.title,JSON.stringify(tables),function(err){
				if(!err){
					console.log('[Error insertBoard] Inserting default values to table');
				}
				res.send(err);
			})
		}
	);
}

exports.deleteBoard = function(req,res){
	if(req.params.boardTitle=='')
	{
		res.send('undefined boardTitle parameter');
	}
	console.log(req.params.boardTitle);
	var result = azure.connector.deleteBoard(req.params.id, req.params.boardTitle,
		function(err){
			if(err!=null){
				console.dir(err);
			}
			res.send(err);
		}
	);
}
