var express = require("express"),
	http = require('http'),
	OAuth = require('oauth').OAuth,
	querystring = require('querystring');
var app = express(),
	account = require('./routes/account.js'),
	api = require('./routes/api.js');
var port = process.env.PORT || 3000;

/*
///<reference path="~/scripts/scene.dataservice.js">
*/
	
function checkAuth(req, res, next) { 
	
    if(typeof req.session.isAutenticated != 'undefined' && req.session.isAutenticated)
	{
		next();
	}
	else
	{
		res.statusCode = 403;
		var e = new Error('You are not authorized to view this page');
		next();
	}
};

app.configure(function(){
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
	app.use(express.cookieParser());
    //app.use(require('connect').bodyParser());
	app.use(express.session({secret: "skjghskdjfhbqigohqdiouk",isAutenticated: false}));
    app.use(express.methodOverride());
    app.use(express.static(__dirname + '/public')); // for static assets (css)
    app.use(express.static(__dirname + '/..')); // for static assets (css)
    app.use(app.router);
	app.use(logErrors);

});

function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}
//test

addToHeader = function (req, res, next) {
  res.contentType('application/json');
  res.header('Access-Control-Allow-Origin' , '*' );
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  console.log("[On request] Added headers to request " + req.url);
  next();
}

app.get('/:id/card/:cardId',addToHeader, api.getCard);
app.post('/:id/card/',addToHeader, api.addCard);
app.post('/:id/tables/:boardTitle',addToHeader, api.updateTable);
app.get('/:id/tables/:boardTitle',addToHeader, api.tables);
app.get('/:id/boards',addToHeader, api.boards);
app.post('/:id/boards',addToHeader, api.insertBoard);
app.delete('/:id/boards/:boardTitle',addToHeader, api.deleteBoard);
app.post('/createUser',addToHeader, account.createUser);
app.post('/login',addToHeader, account.login);

app.get('/api',addToHeader,function (req, res) {
  res.send('Documentation...to do. new one.');
});


app.listen(port);
console.log('server working');